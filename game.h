#ifndef GAME_H
#define GAME_H

#include <memory>
#include <list>
#include <SFML/Graphics.hpp>
#include "utilities/debug.h"
#include "utilities/eventsystem.h"
#include "utilities/timing.h"
#include "common.h"
#include "gameobject.h"
#include "gamestate.h"
#include "gui.h"

enum class ButtonState{
	Up, Down
};

class Game {
public:
	Signal<int, float, float, ButtonState> mousePress;
	Signal<sf::Keyboard::Key, ButtonState> keyPress;
	Signal<int> mouseWheel;

	int uranium = 0;

protected:
	WindowPtr window;
	Timer timeRunning;
	std::shared_ptr<GameState> gameState;
	std::shared_ptr<GameState> newGameState;

	std::list<std::shared_ptr<GameObject>> gameobjects;
	std::list<std::shared_ptr<GUI>> guiWidgets;
	double unscaleddt;
	double scaleddt;

	double targetTimeScale;
	double timeScale;

	double targetSpaceScale; // for tweening
	double spaceScale;

	static void RegisterGameObject(std::shared_ptr<GameObject>);
	static void RegisterGUIWidget(std::shared_ptr<GUI>);
public:
	static std::shared_ptr<Game> $();
	static WindowPtr GetWindow();
	static sf::Font& GetDefaultFont();
	static void SetState(std::shared_ptr<GameState>);
	static void SetTimeScale(double, bool = true);
	static double GetTimeScale(bool = true);
	static void SetScale(double, bool = true);
	static double GetScale(bool = true);

	static double GetUnscaledDeltaTime();
	static double GetDeltaTime();

	static sf::Vector2f GetSize();
	static sf::Vector2f GetHalf();

	//////////////////////////////////////////////////////////
	// ALL GAMEOBJECTS SHOULD BE CREATED WITH THE FOLLOWING //
	// Eg: Game::CreateGameObject<Alien>("Paul", 3);        //
	//////////////////////////////////////////////////////////

	template<class GOType, typename... ArgT>
	static std::shared_ptr<GOType> CreateGameObject(ArgT...);
	template<class GOType, typename... ArgT>
	static std::shared_ptr<GOType> CreateGUIWidget(ArgT...);

	Game();
	~Game();

	void Init();
	void Deinit();
	void Update(double);
	void Draw();
	void ProcessEvents();

	void RunLoop();
	bool IsRunning();
};

class GameState_Start : public GameState{
public:
	Slot<> onStartClick;
	std::shared_ptr<GUI::Button> startButton;

public:
	void Init() override;
	void Deinit() override;
};

#include "building.h"

class Earth;

struct PersistentGameState{
	std::shared_ptr<Earth> earth;
	std::list<std::shared_ptr<Building>> buildings;
};

class GameState_BuildStage : public GameState{
public:
	enum class SelectedAction : int8_t {
		Nothing,
		BuildUMine,
		BuildSilo,
		BuildShield,
		DestroyBuilding
	};

public:
	Slot<int> mouseWheelSlot;
	Slot<double> timeSliderChange;
	Slot<float, float> earthClick;

	Slot<> buildUMineClick;
	Slot<> buildSiloClick;
	Slot<> buildShieldClick;
	Slot<> destroyClick;
	SelectedAction selectedAction = SelectedAction::Nothing;

	double elapsedTime; // in years

	std::shared_ptr<GUI::Button> buildUMineButton;
	std::shared_ptr<GUI::Button> buildSiloButton;
	std::shared_ptr<GUI::Button> buildShieldButton;
	std::shared_ptr<GUI::Button> destroyButton;
	std::shared_ptr<GUI::Slider> timeScaleSlider;

	std::shared_ptr<GUI::Label> yearsPassed;
	std::shared_ptr<GUI::Label> uraniumText;

	std::shared_ptr<PersistentGameState> state;

public:
	void Init() override;
	void Update(double) override;
	void Draw(WindowPtr) override;

	void MouseWheelCallback(int);
	void CreateBuildingCallback(float, float);

	template<class Building_t>
	std::shared_ptr<Building_t> CreateBuildingAt(sf::Vector2f);
};

class Alien;

class GameState_BattleStage : public GameState{
public:
	Slot<int, float, float, ButtonState> mouseClickSlot;
	Slot<int> mouseWheelSlot;
	uint64_t populationRemaining = 7264000000;

	std::shared_ptr<PersistentGameState> state;
	std::list<std::shared_ptr<Alien>> aliens;

	std::shared_ptr<GUI::Label> popRemainingText;
	std::shared_ptr<GUI::Label> uraniumText;

public:
	GameState_BattleStage(std::shared_ptr<PersistentGameState>);

	void Init() override;
	void Deinit() override;
	void Update(double) override;
	void Draw(WindowPtr) override;
	
	void MouseWheelCallback(int);
	void MouseClickCallback(int, float, float, ButtonState);

	float TestLineCircleCollision(sf::Vector2f l1, sf::Vector2f l2, sf::Vector2f c, double r);
};

class GameState_EndGame : public GameState{
public:
	std::shared_ptr<GUI::Label> popSavedText;
	uint64_t populationSaved;

public:
	GameState_EndGame(uint64_t /*remainingPopulation*/);

	void Init() override;
	void Draw(WindowPtr) override;
};

// This has to be here because it's a template
template<class GOType, typename... ArgT>
std::shared_ptr<GOType> Game::CreateGameObject(ArgT... a){
	auto sp = std::make_shared<GOType>(a...);
	Game::RegisterGameObject(std::static_pointer_cast<GameObject>(sp));
	sp->Init();
	return sp;
}

template<class GOType, typename... ArgT>
std::shared_ptr<GOType> Game::CreateGUIWidget(ArgT... a){
	auto sp = std::make_shared<GOType>(a...);
	Game::RegisterGUIWidget(std::static_pointer_cast<GUI>(sp));
	return sp;
}

template<class Building_t>
std::shared_ptr<Building_t> GameState_BuildStage::CreateBuildingAt(sf::Vector2f _pos){
	// Make sure you can afford it
	if(Game::$()->uranium < Building_t::Cost){
		Debug() << "Not Enough Uranium";
		return nullptr;
	}

	// Test for collisions
	for(auto ob : state->buildings){
		if(ob->TestCollision(_pos, 20.f)){
			Debug() << "Something already there";
			return nullptr;
		}
	}

	auto b = Game::CreateGameObject<Building_t>();
	b->isPersistent = true;
	
	b->SetPosition(_pos);
	state->buildings.push_back(b);

	Game::$()->uranium -= Building_t::Cost;

	return b;
}

#endif