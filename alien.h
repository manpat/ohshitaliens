#ifndef ALIEN_H
#define ALIEN_H

#include <memory>
#include "gameobject.h"
#include "common.h"

class Alien : public GameObject{
public:
	static constexpr double deathBeamRate = 10000000.0; // p/s

public:
	static constexpr float descentRate = 80.f; // px/s
	static constexpr float stopRadius = 190.f; // I pulled this out of my ass
	static constexpr float beamWidth = 8.f; // I pulled this out of my ass

	static std::unique_ptr<sf::Texture> tex;
	sf::Sprite sprite;
	sf::RectangleShape beam;

	sf::Vector2f velNormal;
	double rotation;
	float beamDist;

	bool inOrbit = false;

public:
	Alien(sf::Vector2f);

	void Init() override;
	void Update(double) override;
	void Draw(WindowPtr) override;

	bool FirinMahLazor();
	void SetBeamDist(float);

	sf::Vector2f GetPosition();
};

#endif