#include <memory>
#include <cmath>
#include "utilities/debug.h"
#include "common.h"
#include "game.h"

using std::shared_ptr;
using std::make_shared;

int main(){
	try{
		shared_ptr<Game> game = Game::$();
		game->RunLoop();
		
	}catch(const char* e){
		Debug::Error(e);
		Debug::Pause();
	}catch(const std::string& e){
		Debug::Error(e);
		Debug::Pause();
	}catch(const std::exception& e){
		Debug::Error(e.what());
		Debug::Pause();
	}

	return 0;
}