#ifndef MISSILE_H
#define MISSILE_H

#include <memory>
#include "gameobject.h"
#include "common.h"

class Alien;

class Missile : public GameObject {
protected:
	static std::unique_ptr<sf::Texture> tex;

	sf::Sprite sprite;

	std::weak_ptr<Alien> target;
	float rotation;

public:
	Missile(std::weak_ptr<Alien>, sf::Vector2f);

	void Init();
	void Update(double);
	void Draw(WindowPtr);
};

#endif