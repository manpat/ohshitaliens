#ifndef GUI_CPP
#define GUI_CPP

#include "gui.h"
#include "game.h"

using std::string;

GUI::Button::Button(string _label, sf::Vector2f _pos, sf::Vector2f _size){
	quad.setOrigin(_size/2.f);
	quad.setPosition(_pos);
	quad.setSize(_size);
	quad.setFillColor(sf::Color(50.f, 50.f, 50.f));

	label.setString(_label);
	label.setFont(Game::GetDefaultFont());
	label.setPosition(_pos - sf::Vector2f(label.getGlobalBounds().width/2.f, label.getGlobalBounds().height));

	mouseSlot = Slot<int, float, float, ButtonState>::Member(&GUI::Button::Click, this);
	Game::$()->mousePress.Connect(mouseSlot);
}

void GUI::Button::Draw(WindowPtr w){
	w->draw(quad);
	w->draw(label);
}
void GUI::Button::Click(int, float x, float y, ButtonState state){
	if(state == ButtonState::Up) return;

	auto s = Game::GetScale(false);
	sf::Vector2f mpos(x/s, y/s);
	sf::Vector2f extent = quad.getSize()/2.f;
	sf::Vector2f diff = quad.getPosition() - mpos;
	diff.x = abs(diff.x);
	diff.y = abs(diff.y);

	if(diff.x < extent.x && diff.y < extent.y){
		onClick.Raise();
	}
}

GUI::Slider::Slider(double _lb, double _ub, sf::Vector2f _pos, sf::Vector2f _size){
	quad.setOrigin(_size/2.f);
	quad.setPosition(_pos);
	quad.setSize(_size);
	quad.setFillColor(sf::Color(50.f, 50.f, 50.f));

	lowerBound = _lb;
	upperBound = _ub;

	label.setString("NoLabel");
	label.setCharacterSize(18);
	label.setFont(Game::GetDefaultFont());
	label.setOrigin(sf::Vector2f(label.getGlobalBounds().width/2.f, label.getGlobalBounds().height));
	label.setPosition(_pos);

	mouseSlot = Slot<int, float, float, ButtonState>::Member(&GUI::Slider::Click, this);
	Game::$()->mousePress.Connect(mouseSlot);
}

void GUI::Slider::Draw(WindowPtr w){
	w->draw(quad);
	w->draw(label);
}
void GUI::Slider::Click(int, float x, float y, ButtonState state){
	if(state == ButtonState::Up) return;

	auto s = Game::GetScale(false);
	sf::Vector2f mpos(x/s, y/s);
	sf::Vector2f extent = quad.getSize()/2.f;
	sf::Vector2f diff = quad.getPosition() - mpos;
	diff.x += extent.x;
	diff.y = abs(diff.y);

	if((diff.x > 0.f && diff.x <= extent.x*2.f) && diff.y < extent.y){
		double percent = diff.x/(extent.x*2.f);
		value = (1.0 - percent)*upperBound + percent*lowerBound;

		// Debug() << value;
		onChange.Raise(value);
	}
}

double GUI::Slider::GetValue(){
	return value;
}
void GUI::Slider::SetLabel(string _s){
	label.setString(_s);
}

GUI::Label::Label(){
	text.setFont(Game::GetDefaultFont());
	SetJustify(LabelJustify::Left);
	SetCharacterSize(18);
}

void GUI::Label::Draw(WindowPtr w){
	w->draw(text);
}

void GUI::Label::SetJustify(LabelJustify j){
	auto ws = text.getLocalBounds();
	justify = j;

	switch(justify){
		case LabelJustify::Left:{
			text.setOrigin(sf::Vector2f(0.f, ws.height/2.f));
			break;
		}
		case LabelJustify::Center:{
			text.setOrigin(sf::Vector2f(ws.width/2.f, ws.height/2.f));
			break;
		}
		case LabelJustify::Right:{
			text.setOrigin(sf::Vector2f(ws.width, ws.height/2.f));
			break;
		}
	}
}

void GUI::Label::SetPosition(sf::Vector2f p){
	text.setPosition(p);
}

void GUI::Label::SetString(string s){
	text.setString(s);
	SetJustify(justify);
}
void GUI::Label::SetCharacterSize(float s){
	text.setCharacterSize(s);
	SetJustify(justify);
}

#endif