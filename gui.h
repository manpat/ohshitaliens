#ifndef GUI_H
#define GUI_H

#include "utilities/eventsystem.h"
#include <string>
#include "gameobject.h"
#include "common.h"

enum class LabelJustify{
	Left,
	Center,
	Right
};

class GUI{
public:
	class Button;
	class Slider;
	class Label;

	virtual void Update(double) {}
	virtual void Draw(WindowPtr) {}
};

class GUI::Button : public GUI{
public:
	Signal<> onClick;

protected:
	Slot<int, float, float, ButtonState> mouseSlot;
	sf::RectangleShape quad;
	sf::Text label;

	void Click(int, float, float, ButtonState);

public:
	Button(std::string, sf::Vector2f, sf::Vector2f);

	void Draw(WindowPtr) override;
};

class GUI::Slider : public GUI{
public:
	Signal<double> onChange;

protected:
	Slot<int, float, float, ButtonState> mouseSlot;
	sf::RectangleShape quad;
	double upperBound, lowerBound;
	double value;
	sf::Text label;

	void Click(int, float, float, ButtonState);

public:
	Slider(double, double, sf::Vector2f, sf::Vector2f);

	void Draw(WindowPtr) override;

	double GetValue();
	void SetLabel(std::string);
};

class GUI::Label : public GUI{
protected:
	sf::Text text;
	LabelJustify justify;

public:
	Label();

	void Draw(WindowPtr) override;
	
	void SetJustify(LabelJustify);
	void SetPosition(sf::Vector2f);
	void SetString(std::string);
	void SetCharacterSize(float);
};

#endif