#ifndef TIMEUTILS_H
#define TIMEUTILS_H

#include <string>

class Time{
public:
	// Length in seconds
	static constexpr double Second 	= 1.0;
	static constexpr double Minute	= Second*60.0;
	static constexpr double Hour 	= Minute*60.0;
	static constexpr double Day 	= Hour*24.0;
	static constexpr double Year 	= Day*365.0;

	static double Convert(double val, double to, double from = Second);
	static std::string Format(double);
	static std::string FormatFull(double);
};

#endif