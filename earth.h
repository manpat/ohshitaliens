#ifndef EARTH_H
#define EARTH_H

#include "utilities/eventsystem.h"
#include "gameobject.h"

enum class ButtonState;

class Earth : public GameObject {
public:
	Signal<float, float> onClick;

protected:
	sf::Sprite quad;
	std::shared_ptr<sf::Texture> t;
	Slot<int, float, float, ButtonState> mouseSlot;
	float r;

public:
	Earth();

	void Update(double) override;
	void Draw(WindowPtr) override;

	void MouseCallback(int, float, float, ButtonState);
};

#endif