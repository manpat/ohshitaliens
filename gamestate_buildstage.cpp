#ifndef GAMESTATE_BUILDSTAGE
#define GAMESTATE_BUILDSTAGE

#include <cmath>
#include "game.h"
#include "timeutils.h"
#include "earth.h"
#include "building.h"

using std::shared_ptr;
using std::make_shared;

void GameState_BuildStage::Init(){
	state = make_shared<PersistentGameState>();

	mouseWheelSlot = Slot<int>::Member(&GameState_BuildStage::MouseWheelCallback, this);
	Game::$()->mouseWheel.Connect(mouseWheelSlot);

	// Game::SetTimeScale(Time::Day*2.0);
	Game::SetTimeScale(1.0);
	Game::SetScale(1.0);

	earthClick = Slot<float, float>::Member(&GameState_BuildStage::CreateBuildingCallback, this);
	state->earth = Game::CreateGameObject<Earth>();
	state->earth->isPersistent = true;
	state->earth->onClick.Connect(earthClick);

	sf::Vector2f bsize(180.f, 50.f);
	sf::Vector2f bpos = Game::GetHalf();
	bpos.y -= 40.f;
	bpos.x = -300.f;
	buildUMineButton = Game::CreateGUIWidget<GUI::Button>("Build UMine", bpos, bsize);
	bpos.x = -100.f;
	buildSiloButton = Game::CreateGUIWidget<GUI::Button>("Build NukeSilo", bpos, bsize);
	bpos.x = 100.f;
	buildShieldButton = Game::CreateGUIWidget<GUI::Button>("Build Shield", bpos, bsize);
	bpos.x = 300.f;
	destroyButton = Game::CreateGUIWidget<GUI::Button>("Destroy Building", bpos, bsize);

	buildUMineClick = Slot<>([this](){
		selectedAction = SelectedAction::BuildUMine;
		Debug() << "Selected: Build UMine";
	});
	buildSiloClick = Slot<>([this](){
		selectedAction = SelectedAction::BuildSilo;
		Debug() << "Selected: Silo";
	});
	buildShieldClick = Slot<>([this](){
		selectedAction = SelectedAction::BuildShield;
		Debug() << "Selected: Shield";
	});
	destroyClick = Slot<>([this](){
		selectedAction = SelectedAction::DestroyBuilding;
		Debug() << "Selected: Destroy";
	});

	buildUMineButton->onClick.Connect(buildUMineClick);
	buildSiloButton->onClick.Connect(buildSiloClick);
	buildShieldButton->onClick.Connect(buildShieldClick);
	destroyButton->onClick.Connect(destroyClick);

	bsize.x = Game::GetHalf().x;
	bsize.y = 25.f;
	bpos = Game::GetHalf();
	bpos.x = bpos.x-bsize.x/2.f-6.f;
	bpos.y = -bpos.y+18.f;
	timeScaleSlider = Game::CreateGUIWidget<GUI::Slider>(log10(Time::Second), log10(Time::Year*2.0), bpos, bsize);

	timeSliderChange = Slot<double>([this](double v){
		double _ts = pow(10.0, v);
		Game::SetTimeScale(_ts);
	});
	timeScaleSlider->onChange.Connect(timeSliderChange);

	yearsPassed = Game::CreateGUIWidget<GUI::Label>();
	yearsPassed->SetCharacterSize(18);
	yearsPassed->SetPosition(-Game::GetHalf() + sf::Vector2f(0.f, 10.f));

	uraniumText = Game::CreateGUIWidget<GUI::Label>();
	uraniumText->SetCharacterSize(18);
	uraniumText->SetPosition(sf::Vector2f(-Game::GetHalf().x+10.f, Game::GetHalf().y-90.f));

	Game::$()->uranium = 20;
}

void GameState_BuildStage::Update(double dt){
	elapsedTime += dt;

	double timeRemaining = 10.0*Time::Year - elapsedTime;
	if(timeRemaining <= 0.0){
		Game::SetTimeScale(1.0);
		Game::SetScale(1.0);
		Game::SetState(make_shared<GameState_BattleStage>(state));
		return;

	}else if(timeRemaining < Game::GetTimeScale()){
		Game::SetTimeScale(timeRemaining + 1.0);
	}

	yearsPassed->SetString(Time::FormatFull(elapsedTime));
	uraniumText->SetString(Debug::Append() << "Uranium:" << Game::$()->uranium);

	if(abs(Game::GetTimeScale() - 1.0) == 0.0){
		timeScaleSlider->SetLabel("Realtime");
	}else{
		timeScaleSlider->SetLabel(Debug::Append(false) 
			<< Time::Format(Game::GetTimeScale()) << "/s");
	}
}

void GameState_BuildStage::Draw(WindowPtr w){
	// w->draw(yearsPassed);
	// w->draw(uraniumText);
}

void GameState_BuildStage::MouseWheelCallback(int delta){
	double ss = Game::GetScale();

	double ssexp = log10(ss);
	ssexp -= delta*0.2;
	ss = pow(10, ssexp);

	Game::SetScale(ss);
}

void GameState_BuildStage::CreateBuildingCallback(float x, float y){
	sf::Vector2f p(x, y);

	switch(selectedAction){
		case SelectedAction::BuildSilo:
			CreateBuildingAt<NukeSilo>(p);
			break;
		case SelectedAction::BuildUMine:
			CreateBuildingAt<UraniumMine>(p);
			break;
		case SelectedAction::BuildShield:
			CreateBuildingAt<ShieldGenerator>(p);
			break;

		case SelectedAction::DestroyBuilding:{
			for(auto b : state->buildings){
				if(b->TestCollision(p)) {
					b->isWillingToDie = true;
					Game::$()->uranium += b->Cost/2;
				}
			}

			state->buildings.remove_if(
				[](shared_ptr<Building>& x){
					return x->isWillingToDie;
				});
			break;
		}
		
		default:
			break;
	}
}

#endif