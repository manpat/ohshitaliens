#ifndef TIMEUTILS_CPP
#define TIMEUTILS_CPP

#include "timeutils.h"
#include "common.h"
#include "utilities/debug.h"

using std::string;
using std::to_string;

double Time::Convert(double val, double to, double from){
	return val*from/to;
}

string Time::Format(double val){
	string unit = "s";

	if(val >= Time::Year){
		val /= Time::Year;
		unit = "year";
	}else if(val >= Time::Day){
		val /= Time::Day;
		unit = "day";
	}else if(val >= Time::Hour){
		val /= Time::Hour;
		unit = "hour";
	}else if(val >= Time::Minute){
		val /= Time::Minute;
		unit = "minute";
	}else if(val >= Time::Second){
		val /= Time::Second;
		unit = "second";
	}

	if(val > 1.0) unit += "s";

	return Debug::Append(false) << roundto(val, 0.1) << unit;
}

string Time::FormatFull(double val){
	int years = roundto(Time::Convert(val, Time::Year)); val -= years*Time::Year;
	int days = roundto(Time::Convert(val, Time::Day)); val -= days*Time::Day;
	int hours = roundto(Time::Convert(val, Time::Hour)); val -= hours*Time::Hour;
	int minutes = roundto(Time::Convert(val, Time::Minute)); val -= minutes*Time::Minute;
	int seconds = roundto(val);
	int tenthseconds = (val - seconds)*10.0;

	string formated = to_string(seconds) + "." + to_string(tenthseconds) + "s";

	if(minutes > 0.0){
		formated = to_string(minutes) + "minute" + (minutes>1?"s ":" ") + formated;
	}

	if(hours > 0.0){
		formated = to_string(hours) + "hour" + (hours>1?"s ":" ") + formated;
	}

	if(days > 0.0){
		formated = to_string(days) + "day" + (days>1?"s ":" ") + formated;
	}

	if(years > 0.0){
		formated = to_string(years) + "year" + (years>1?"s ":" ") + formated;
	}

	return formated;
}

#endif