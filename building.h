#ifndef BUILDING_H
#define BUILDING_H

#include <memory>
#include "common.h"
#include "gameobject.h"
#include "timeutils.h"

class Alien;

class Building : public GameObject{
public:
	static constexpr int Cost = 10;

	enum class Type {
		Silo, Mine, Shield
	};
	const Type type;

	bool isBuilt = false; // Has been fully built and has not been scheduled to be destroyed
	bool isActive = false; // isBuilt && in battlestage

protected:
	static std::unique_ptr<sf::Texture> unbuiltSpriteTex;
	sf::Sprite sprite;

	double timeTillBuilt; // Set in init; countdown in update

public:
	Building(Type);
	void Update(double) override;
	void Draw(WindowPtr) override;

	void SetPosition(sf::Vector2f);
	void SetTexture(std::unique_ptr<sf::Texture>&);

	sf::Vector2f GetPosition();

	virtual void BuildFinished() {};
	virtual void EnableBattleMode() {};

	bool TestCollision(sf::Vector2f, float = 0.f);
};

class NukeSilo : public Building {
protected:
	static std::unique_ptr<sf::Texture> builtSpriteTex;

	const double fireArc = M_PI*2.0/3.0;
	const int missileCost = 1;
	double cooldown = 15.f;
	double cooldownTimer;
	double fireArcCenter;

public:
	NukeSilo();

	void Init() override;
	void Update(double) override;
	void BuildFinished() override;
	void EnableBattleMode() override;

	void LaunchAt(std::shared_ptr<Alien>);
};

class UraniumMine : public Building {
protected:
	static std::unique_ptr<sf::Texture> builtSpriteTex;

	const double mineInterval = Time::Year/3.0;
	const int UPerCollect = 3;

	double timeTillCollect;

public:
	UraniumMine();

	void Init() override;
	void Update(double) override;
	void BuildFinished() override;
};

class ShieldGenerator : public Building {
public:
	static constexpr float protectionRadius = 40.f;
	static constexpr double drainTick = 5.0/*Time::Second*/;
	static constexpr int drainAmount = 1;
	bool isPowered = false;

protected:
	static std::unique_ptr<sf::Texture> builtSpriteTex;
	sf::CircleShape bubble;

	double timeTillDrain;

public:
	ShieldGenerator();

	void Init() override;
	void Update(double) override;
	void Draw(WindowPtr) override;
	void BuildFinished() override;
	void EnableBattleMode() override;
};

#endif