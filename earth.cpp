#ifndef EARTH_CPP
#define EARTH_CPP

#include <cmath>
#include "game.h"
#include "earth.h"
#include "timeutils.h"

Earth::Earth(){
	t = std::make_shared<sf::Texture>();
	if(!t->loadFromFile("images/planet.png")){
		throw("Earth texture load failed");
	}
	quad.setTexture(*t.get());

	float scale = 2.f;

	sf::Vector2f size(t->getSize());

	quad.setScale(scale, scale);
	quad.setOrigin(size/2.f);
	quad.setPosition(sf::Vector2f());

	r = size.x*scale/2.f;

	mouseSlot = Slot<int, float, float, ButtonState>::Member(&Earth::MouseCallback, this);
	Game::$()->mousePress.Connect(mouseSlot);
}

void Earth::Update(double dt){
	quad.rotate(Time::Convert(dt, Time::Year) * 360.0); // One rotation per year
}

void Earth::Draw(WindowPtr window){
	window->draw(quad);
}

void Earth::MouseCallback(int button, float x, float y, ButtonState state){
	if(state == ButtonState::Up) return;

	auto diff = sf::Vector2f(x, y) - quad.getPosition();
	float d = sqrt(diff.x*diff.x + diff.y*diff.y);
	if(d < r){
		onClick.Raise(x, y);
	}
}

#endif
