#ifndef GAME_CPP
#define GAME_CPP

#include "game.h"
#include "timeutils.h"

using std::weak_ptr;
using std::shared_ptr;
using std::make_shared;

shared_ptr<Game> Game::$(){
	static weak_ptr<Game> _$;
	if(auto sp = _$.lock()){
		return sp;
	}
	return (_$ = make_shared<Game>()).lock();
}

Game::Game(){
	window = make_shared<sf::RenderWindow>(sf::VideoMode(800.f, 600.f), "Oh Shit! Aliens!");
	targetTimeScale = 1.0;
	timeScale = 0.0;
	targetSpaceScale = 1.0;
	spaceScale = 10.0;

	sf::View view = window->getView();
	view.setCenter(sf::Vector2f());
	window->setView(view);
}

Game::~Game(){

}

void Game::RegisterGameObject(shared_ptr<GameObject> o){
	$()->gameobjects.push_back(o);
}
void Game::RegisterGUIWidget(shared_ptr<GUI> o){
	$()->guiWidgets.push_back(o);
}

WindowPtr Game::GetWindow(){
	return $()->window;
}

sf::Font& Game::GetDefaultFont(){
	static shared_ptr<sf::Font> font;
	if(!font){
		font = make_shared<sf::Font>();
		if(!font->loadFromFile("arial.ttf")) 
			throw("Default font load failed");
	}

	return *font;
}

void Game::SetState(shared_ptr<GameState> _state){
	$()->newGameState = _state;
}

void Game::SetTimeScale(double _ts, bool tween){
	$()->targetTimeScale = _ts;
	if(!tween) $()->timeScale = _ts;
}

double Game::GetTimeScale(bool tween){
	if(tween)
		return $()->targetTimeScale;
	return $()->timeScale;
}

void Game::SetScale(double _ss, bool tween){
	$()->targetSpaceScale = _ss;
	if(!tween) $()->spaceScale = _ss;
}
double Game::GetScale(bool tween){
	if(tween)
		return $()->targetSpaceScale;
	return $()->spaceScale;
}

sf::Vector2f Game::GetSize(){
	return sf::Vector2f($()->window->getSize());
}

sf::Vector2f Game::GetHalf(){
	return GetSize()/2.f;
}

double Game::GetUnscaledDeltaTime(){
	return $()->unscaleddt;
}
double Game::GetDeltaTime(){
	return $()->scaleddt;
}

void Game::Init(){
	if(!gameState) return;

	gameState->Init();

	for(auto go : gameobjects){
		go->Init();
	}
}

void Game::Deinit(){
	if(!gameState) return;

	gameState->Deinit();

	for(auto go : gameobjects){
		go->Deinit();
	}
}

void Game::Update(double dt){
	if(!gameState) return;

	gameState->Update(dt);

	for(auto go : gameobjects){
		go->Update(dt);
	}
	for(auto gw : guiWidgets){
		gw->Update(dt);
	}

	// Below is just tweening
	timeScale += (targetTimeScale - timeScale)*unscaleddt*4.0;
	spaceScale += (targetSpaceScale - spaceScale)*unscaleddt*4.0;
}

void Game::Draw(){
	if(!gameState) return;

	window->clear();

	sf::View view = window->getView();
	sf::Vector2f size(window->getSize());

	gameState->Draw(window);

	for(auto go : gameobjects){
		go->Draw(window);
	}

	view.setSize(size);
	window->setView(view);

	for(auto gw : guiWidgets){
		gw->Draw(window);
	}

	view.setSize(size*(float)spaceScale);
	window->setView(view);
	window->display();
}

void Game::ProcessEvents(){
	sf::Event event;
	while(window->pollEvent(event)){
		if(event.type == sf::Event::Closed){
			window->close();
		}else if(event.type == sf::Event::KeyPressed){
			if(event.key.code == sf::Keyboard::Escape){
				window->close();
			}

			keyPress.Raise(event.key.code, ButtonState::Down);

		}else if(event.type == sf::Event::MouseButtonPressed){
			sf::Vector2f mpos = window->mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
			mousePress.Raise((int)event.mouseButton.button, mpos.x, mpos.y, ButtonState::Down);

		}else if(event.type == sf::Event::MouseButtonReleased){
			sf::Vector2f mpos = window->mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
			mousePress.Raise((int)event.mouseButton.button, mpos.x, mpos.y, ButtonState::Up);

		}else if(event.type == sf::Event::MouseWheelMoved){
			mouseWheel.Raise(event.mouseWheel.delta);
		}
	}
}

void Game::RunLoop(){
	double t = 0.f;

	SetState(make_shared<GameState_Start>());

	while(IsRunning()){
		if(gameState != newGameState){
			Deinit();
			gameobjects.remove_if([](shared_ptr<GameObject>& x){
				return !x || !x->isPersistent;
			});
			guiWidgets.clear();
			gameState = newGameState;
			Init();
		}

		unscaleddt = timeRunning.Get() - t;
		t = timeRunning.Get();
		scaleddt = unscaleddt*timeScale;

		ProcessEvents();

		Update(scaleddt);
		Draw();

		gameobjects.remove_if([](shared_ptr<GameObject>& x){
			return !x || x->isWillingToDie;
		});
	}
}

bool Game::IsRunning(){
	return window->isOpen();
}

#endif