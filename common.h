#ifndef COMMON_H
#define COMMON_H

#include <SFML/Graphics.hpp>
#include <memory>

typedef std::shared_ptr<sf::RenderWindow> WindowPtr;

// Defined in game.h
enum class ButtonState;

// Defined in mathstuff.cpp
double roundto(double x, double to = 1.0);
double clamp(double x, double lower = 0.0, double upper = 1.0);

float dot(sf::Vector2f a, sf::Vector2f b);
float mag(sf::Vector2f a);
float dist(sf::Vector2f a, sf::Vector2f b);

#include <cmath>
#ifndef M_PI
#define M_PI 3.141592654
#endif

#endif