#ifndef ALIEN_CPP
#define ALIEN_CPP

#include "alien.h"
#include "common.h"
#include <cmath>

using std::unique_ptr;

unique_ptr<sf::Texture> Alien::tex = nullptr;

Alien::Alien(sf::Vector2f _pos){
	if(!tex){
		tex = unique_ptr<sf::Texture>(new sf::Texture());
		tex->loadFromFile("images/alien.png");
	}

	sprite.setTexture(*tex);
	sprite.setOrigin(sf::Vector2f(tex->getSize())/2.f);
	sprite.setPosition(_pos);

	rotation = atan2(_pos.y, _pos.x); // This is because earth is at (0, 0)
	sprite.setRotation(rotation*180.0/M_PI + 90.0);

	velNormal = -sf::Vector2f(cos(rotation), sin(rotation)) * descentRate;
}

void Alien::Init(){

}

void Alien::Update(double dt){
	sf::Vector2f p = sprite.getPosition();
	double dist = sqrt(p.x*p.x + p.y*p.y); 
	// I simplified out the difference here because it's
	//		assumed that earth is at (0, 0)

	if(dist > stopRadius){ 
		sprite.move(velNormal*(float)dt);
	}else if(!inOrbit){
		inOrbit = true;

		beam.setOrigin(sf::Vector2f(0.f, beamWidth/2.f));
		beam.setPosition(p);
		beam.setRotation(rotation*180.0/M_PI + 180.f);
		beam.setFillColor(sf::Color(180.f, 50.f, 140.f));

		beam.setOutlineThickness(1.f);
		beam.setOutlineColor(sf::Color(150.f, 40.f, 100.f));
	}
}

void Alien::Draw(WindowPtr w){
	if(inOrbit)
		w->draw(beam);
	w->draw(sprite);
}

bool Alien::FirinMahLazor(){
	return inOrbit && !isWillingToDie;
}

void Alien::SetBeamDist(float _dist){
	beamDist = _dist;
	beam.setSize(sf::Vector2f(beamDist, beamWidth));
}

sf::Vector2f Alien::GetPosition(){
	return sprite.getPosition();
}

#endif	