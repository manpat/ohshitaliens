#ifndef GAMESTATE_ENDGAME
#define GAMESTATE_ENDGAME

#include "game.h"

GameState_EndGame::GameState_EndGame(uint64_t _remainingPop)
	: populationSaved(_remainingPop) {

}

void GameState_EndGame::Init(){
	popSavedText = Game::CreateGUIWidget<GUI::Label>();
	popSavedText->SetPosition(sf::Vector2f(0, 0));
	popSavedText->SetJustify(LabelJustify::Center);
	popSavedText->SetString(Debug::Append()
		<< "Population Not Dead Yet:" << populationSaved);
	popSavedText->SetCharacterSize(40);
}

void GameState_EndGame::Draw(WindowPtr w){
	// w->draw(popSavedText);
}

#endif