#ifndef GAMESTATE_START
#define GAMESTATE_START

#include <cmath>
#include "game.h"

using std::make_shared;

void GameState_Start::Init(){
	startButton = Game::CreateGUIWidget<GUI::Button>("Start", sf::Vector2f(), sf::Vector2f(200.f, 100.f));
	onStartClick = Slot<>([](){
		Game::SetState(make_shared<GameState_BuildStage>());
	});

	startButton->onClick.Connect(onStartClick);
}

void GameState_Start::Deinit(){
	Game::SetScale(100.0, false);
}

#endif