#ifndef BUILDING_CPP
#define BUILDING_CPP

#include "utilities/debug.h"
#include "building.h"
#include "game.h"

using std::unique_ptr;
using std::shared_ptr;

unique_ptr<sf::Texture> Building::unbuiltSpriteTex = nullptr;
unique_ptr<sf::Texture> NukeSilo::builtSpriteTex = nullptr;
unique_ptr<sf::Texture> UraniumMine::builtSpriteTex = nullptr;
unique_ptr<sf::Texture> ShieldGenerator::builtSpriteTex = nullptr;

//////////////////////////////////////////////////////////////////////////
//                                                                      //
//          Building Base                                               //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

Building::Building(Type _type)
	: type(_type){
	if(!unbuiltSpriteTex){
		unbuiltSpriteTex = unique_ptr<sf::Texture>(new sf::Texture());
		unbuiltSpriteTex->loadFromFile("images/construction.png");
	}

	SetTexture(unbuiltSpriteTex);
}

void Building::Update(double dt){
	if(!isBuilt){
		timeTillBuilt -= dt;
		
		if(timeTillBuilt <= 0.0) {
			isBuilt = true;
			BuildFinished();
		}
	}
}

void Building::Draw(WindowPtr w){
	w->draw(sprite);
}

void Building::SetPosition(sf::Vector2f _pos){
	sprite.setPosition(_pos);
}
void Building::SetTexture(unique_ptr<sf::Texture>& _tex){
	sprite.setTexture(*_tex);

	sf::Vector2f s(_tex->getSize());
	sprite.setOrigin(sf::Vector2f(s.x/2.f, s.y));

	sprite.setTextureRect(
		sf::IntRect{0, 0, (int)floor(s.x), (int)floor(s.y)});
}

sf::Vector2f Building::GetPosition(){
	return sprite.getPosition();
}

bool Building::TestCollision(sf::Vector2f point, float leeway){
	sf::Vector2f diff = sprite.getPosition() - point;
	diff.x = abs(diff.x);
	diff.y = abs(diff.y);

	auto bounds = sprite.getLocalBounds();
	sf::Vector2f extent = sf::Vector2f(bounds.width + leeway, bounds.height + leeway)/2.f;

	return (diff.x < extent.x) && (diff.y < extent.y);
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
//          Nuke Silo                                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "alien.h"
#include "missile.h"

NukeSilo::NukeSilo()
	: Building(Type::Silo){
	if(!builtSpriteTex){
		builtSpriteTex = unique_ptr<sf::Texture>(new sf::Texture());
		builtSpriteTex->loadFromFile("images/silo.png");
	}
}

void NukeSilo::Init(){
	if(isInit) return;
	isInit = true;

	timeTillBuilt = Time::Year;
}

void NukeSilo::Update(double dt){
	Building::Update(dt);
	if(!isActive) {
		cooldown -= dt/Time::Year;
		return;
	}

	cooldownTimer -= dt;
}

void NukeSilo::BuildFinished(){
	SetTexture(builtSpriteTex);
}

void NukeSilo::EnableBattleMode(){
	isActive = isBuilt;

	auto p = GetPosition();
	fireArcCenter = atan2(p.y, p.x);
}

void NukeSilo::LaunchAt(shared_ptr<Alien> a){
	int& U = Game::$()->uranium;

	if(U < missileCost || cooldownTimer > 0.0) return;

	auto ap = a->GetPosition();
	double angleFromEarth = atan2(ap.y, ap.x);
	angleFromEarth = 2.0*fabs(angleFromEarth - fireArcCenter)/fireArc;
	if(angleFromEarth > 1.0) return;

	U -= missileCost;
	cooldownTimer = cooldown;

	Game::CreateGameObject<Missile>(a, GetPosition());
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
//          Uranium Mine                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

UraniumMine::UraniumMine()
	: Building(Type::Mine){
	if(!builtSpriteTex){
		builtSpriteTex = unique_ptr<sf::Texture>(new sf::Texture());
		builtSpriteTex->loadFromFile("images/mine.png");
	}
}

void UraniumMine::Init(){
	if(isInit) return;
	isInit = true;

	timeTillBuilt = Time::Year;
}

void UraniumMine::Update(double dt){
	Building::Update(dt);
	if(!isActive) return;

	timeTillCollect -= dt;

	if(timeTillCollect <= 0.0){
		Game::$()->uranium += UPerCollect;
		Debug() << "Uranium collected";

		timeTillCollect = mineInterval;
	}
}

void UraniumMine::BuildFinished(){
	SetTexture(builtSpriteTex);
	isActive = true;
	timeTillCollect = mineInterval;
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
//          Shield Generator                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ShieldGenerator::ShieldGenerator()
	: Building(Type::Shield){
	if(!builtSpriteTex){
		builtSpriteTex = unique_ptr<sf::Texture>(new sf::Texture());
		builtSpriteTex->loadFromFile("images/gen_off.png");
	}
}

void ShieldGenerator::Init(){
	if(isInit) return;
	isInit = true;

	timeTillBuilt = Time::Year;
}

void ShieldGenerator::Update(double dt){
	Building::Update(dt);
	if(!isActive) return;

	int& uranium = Game::$()->uranium;

	if(!isPowered){
		if(uranium >= drainAmount){
			uranium -= drainAmount;
			timeTillDrain = drainTick;
			isPowered = true;
		}else{
			return;
		}
	}
	
	timeTillDrain -= dt;

	if(timeTillDrain <= 0.0){
		if(uranium < drainAmount){
			isPowered = false;
			return;
		}

		uranium -= drainAmount;
		timeTillDrain = drainTick;
	}
}

void ShieldGenerator::Draw(WindowPtr w){
	Building::Draw(w);
	if(isPowered)
		w->draw(bubble);
}

void ShieldGenerator::BuildFinished(){
	SetTexture(builtSpriteTex);
}

void ShieldGenerator::EnableBattleMode(){
	isActive = isBuilt;
	sf::Vector2f s(builtSpriteTex->getSize());

	bubble.setRadius(protectionRadius);
	bubble.setOrigin(sf::Vector2f(protectionRadius, protectionRadius));
	bubble.setPosition(GetPosition());
	bubble.setFillColor(sf::Color(30.f, 30.f, 100.f, 100.f));
	bubble.setOutlineColor(sf::Color(30.f, 30.f, 200.f, 150.f));
	bubble.setOutlineThickness(3.f);

	timeTillDrain = drainTick;
}

#endif