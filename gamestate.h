#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "common.h"

class GameState {
public:
	virtual void Init() {};
	virtual void Deinit() {};
	virtual void Update(double) {};
	virtual void Draw(WindowPtr) {};
};

#endif