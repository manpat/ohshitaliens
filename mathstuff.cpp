#ifndef MATHSTUFF_CPP
#define MATHSTUFF_CPP

#include "common.h"
#include <cmath>

double roundto(double x, double to){
	return floor(x/to)*to;
}

double clamp(double x, double lower, double upper){
	return std::max(std::min(x, upper), lower);
}

float dot(sf::Vector2f a, sf::Vector2f b){
	return a.x*b.x + a.y*b.y;
}

float mag(sf::Vector2f a){
	return sqrt(a.x*a.x + a.y*a.y);
}

float dist(sf::Vector2f a, sf::Vector2f b){
	auto d = a-b;
	return sqrt(d.x*d.x + d.y*d.y);
}

#endif