#ifndef MISSILE_CPP
#define MISSILE_CPP

#include "utilities/debug.h"
#include "missile.h"
#include "alien.h"

using std::unique_ptr;
using std::weak_ptr;

unique_ptr<sf::Texture> Missile::tex = nullptr;

Missile::Missile(std::weak_ptr<Alien> a, sf::Vector2f pos)
	: target(a) {

	if(!tex){
		tex = unique_ptr<sf::Texture>(new sf::Texture());
		tex->loadFromFile("images/missile.png");
	}

	sprite.setPosition(pos);
}

void Missile::Init(){
	sf::Vector2f tsize(tex->getSize());
	tsize = tsize/2.f;
	sf::Vector2f pos = sprite.getPosition();

	sprite.setTexture(*tex);
	sprite.setOrigin(tsize/2.f);
	sprite.setPosition(pos);

	rotation = atan2(pos.y, pos.x);
	sprite.setRotation(rotation * 180.f/M_PI);
}

void Missile::Update(double dt){
	if(auto a = target.lock()){
		sf::Vector2f ap = a->GetPosition();
		sf::Vector2f mp = sprite.getPosition();
		auto d = ap-mp;

		if(mag(d) < 45.f){
			a->isWillingToDie = true;
			isWillingToDie = true;
		}

		rotation = atan2(d.y, d.x);
		sprite.setRotation(rotation * 180.f/M_PI);
	}

	const float speed = 70.f;

	sf::Vector2f vel(cos(rotation)*speed*dt, sin(rotation)*speed*dt);
	sprite.move(vel);

}

void Missile::Draw(WindowPtr w){
	w->draw(sprite);
}

#endif