#ifndef GAMESTATE_BATTLESTAGE
#define GAMESTATE_BATTLESTAGE

#include <cmath>
#include <climits>
#include "game.h"
#include "earth.h"
#include "alien.h"

using std::shared_ptr;
using std::make_shared;
using std::static_pointer_cast;
using limit = std::numeric_limits<float>;

GameState_BattleStage::GameState_BattleStage(shared_ptr<PersistentGameState> _state)
	: state(_state) {

}

void GameState_BattleStage::Init(){
	mouseWheelSlot = Slot<int>::Member(&GameState_BattleStage::MouseWheelCallback, this);
	Game::$()->mouseWheel.Connect(mouseWheelSlot);

	mouseClickSlot = Slot<int, float, float, ButtonState>::Member(
		&GameState_BattleStage::MouseClickCallback, this);
	Game::$()->mousePress.Connect(mouseClickSlot);

	popRemainingText = Game::CreateGUIWidget<GUI::Label>();
	popRemainingText->SetCharacterSize(30);
	popRemainingText->SetPosition(sf::Vector2f(-Game::GetHalf().x+10.f, -Game::GetHalf().y+10.f));

	uraniumText = Game::CreateGUIWidget<GUI::Label>();
	uraniumText->SetCharacterSize(18);
	uraniumText->SetPosition(sf::Vector2f(-Game::GetHalf().x+10.f, Game::GetHalf().y-90.f));
	
	for(double theta = 0.0; theta < 2.0*M_PI; theta += M_PI/8.0){
		aliens.push_back(
			Game::CreateGameObject<Alien>(sf::Vector2f(cos(theta), sin(theta))*500.f));
	}

	for(auto b : state->buildings){
		b->EnableBattleMode(); // Turns on shields, arms missiles
	}
}

void GameState_BattleStage::Deinit(){
	for(auto b : state->buildings){
		b->isWillingToDie = true;
	}

	state->earth->isWillingToDie = true;
}

void GameState_BattleStage::Update(double dt){
	if(aliens.size() == 0){
		Game::SetState(make_shared<GameState_EndGame>(populationRemaining));
	}

	aliens.remove_if([](shared_ptr<Alien>& a){
		return a->isWillingToDie;
	});

	double peopleKilledThisTick = 0;
	for(auto a : aliens){
		if(a->FirinMahLazor()){
			sf::Vector2f pos = a->GetPosition();
			sf::Vector2f zero(0.f, 0.f);
			float dist = limit::infinity();

			for(auto b : state->buildings){
				if(b->type != Building::Type::Shield) continue;

				auto sg = static_pointer_cast<ShieldGenerator>(b);
				if(!sg->isPowered) continue; // Out of power?

				dist = std::min(dist,
					TestLineCircleCollision(pos, zero, 
						b->GetPosition(), ShieldGenerator::protectionRadius));
			}

			float d2earth = TestLineCircleCollision(pos, zero, 
						zero, 128.f);

			if(d2earth < dist){
				peopleKilledThisTick += Alien::deathBeamRate*dt;
				a->beam.setFillColor(sf::Color::Red);
			}else{
				a->beam.setFillColor(sf::Color::Blue);
			}

			dist = std::min(dist, d2earth);
			if(dist < limit::infinity())
				a->SetBeamDist(dist);
		}
	}

	if(peopleKilledThisTick > populationRemaining){
		populationRemaining = 0;
		Game::SetState(make_shared<GameState_EndGame>(0));
	}else{
		populationRemaining -= (int)floor(peopleKilledThisTick);
	}

	popRemainingText->SetString(
		Debug::Append() << "Population Not Dead:" << populationRemaining);
	
	uraniumText->SetString(Debug::Append() << "Uranium:" << Game::$()->uranium);
}

void GameState_BattleStage::Draw(WindowPtr w){
	// w->draw(popRemainingText);
	// w->draw(uraniumText);
}

void GameState_BattleStage::MouseWheelCallback(int delta){
	double ss = Game::GetScale();

	double ssexp = log10(ss);
	ssexp -= delta*0.2;
	ss = pow(10, ssexp);

	Game::SetScale(ss);
}

void GameState_BattleStage::MouseClickCallback(int, float x, float y, ButtonState bstate){
	if(bstate == ButtonState::Up) return;

	sf::Vector2f p(x, y);

	const float alienSize = 40.f;

	for(auto a : aliens){
		auto ap = a->GetPosition();
		float d = dist(p, ap);
		if(d < alienSize){
			for(auto s : state->buildings){
				if(s->type != Building::Type::Silo) continue;
				if(!s->isActive) continue;

				auto silo = static_pointer_cast<NukeSilo>(s);
				silo->LaunchAt(a);
			}
		}
	}
}

float GameState_BattleStage::TestLineCircleCollision(sf::Vector2f A, sf::Vector2f B, 
	sf::Vector2f C, double r){

	sf::Vector2f AB = B - A;
	sf::Vector2f AC = C - A;

	float magAB = mag(AB);
	float magAC = mag(AC);

	float costheta = dot(AB, AC)/(magAB*magAC); 

	// v  |cdir| cos(theta)
	float lineInt = costheta*magAC; //dot(ldir, cdir)/magAB;

	if(lineInt > magAB){
		return limit::infinity();
	}

	float theta = acos(costheta);
	float dist = magAC*sin(theta);

	if(dist > r){
		return limit::infinity();
	}
	return lineInt - r; // hacky but whatever
}

#endif