#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <memory>
#include <SFML/Graphics.hpp>
#include "utilities/eventsystem.h"
#include "common.h"

class GameObject{
public:
	bool isPersistent = false;
	bool isWillingToDie = false;
	bool isInit = false;

public:
	virtual void Init() {}
	virtual void Deinit() {}
	virtual void Update(double) {}
	virtual void Draw(WindowPtr) {}
};

#endif